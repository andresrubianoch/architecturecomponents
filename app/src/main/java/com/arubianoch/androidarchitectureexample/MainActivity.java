package com.arubianoch.androidarchitectureexample;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.arubianoch.androidarchitectureexample.adapters.NoteAdapter;
import com.arubianoch.androidarchitectureexample.adapters.NoteListAdapter;
import com.arubianoch.androidarchitectureexample.entity.Note;
import com.arubianoch.androidarchitectureexample.viewModel.NoteViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private NoteViewModel noteViewModel;
    private static final int ADD_NOTE_REQUEST = 1;
    private static final int EDIT_NOTE_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.addNote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
                startActivityForResult(intent, ADD_NOTE_REQUEST);
            }
        });

        final RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        Intent intent = getIntent();
        final boolean recyclerHolderType = intent.hasExtra(ChooseAdapterTypeActivity.EXTRA_HOLDER_TYPE);

        final NoteAdapter noteAdapter = new NoteAdapter();
        final NoteListAdapter noteListAdapter = new NoteListAdapter();
        recyclerView.setAdapter(noteAdapter);

        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        noteViewModel.getAllNotes().observe(this, new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                if (recyclerHolderType) {
                    noteAdapter.setNoteList(notes);
                } else {
                    noteListAdapter.submitList(notes);
                }
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                if (recyclerHolderType) {
                    noteViewModel.delete(noteAdapter.getNoteAt(viewHolder.getAdapterPosition()));
                } else {
                    noteViewModel.delete(noteListAdapter.getNoteAt(viewHolder.getAdapterPosition()));
                }
                Toast.makeText(MainActivity.this, "Note deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);

        if (recyclerHolderType) {

            noteAdapter.setOnItemClickListener(new NoteAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Note note) {
                    Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
                    intent.putExtra(AddEditNoteActivity.EXTRA_ID, note.getId());
                    intent.putExtra(AddEditNoteActivity.EXTRA_TITLE, note.getTitle());
                    intent.putExtra(AddEditNoteActivity.EXTRA_DESCRIPTION, note.getDescription());
                    intent.putExtra(AddEditNoteActivity.EXTRA_PRIORITY, note.getPriority());
                    startActivityForResult(intent, EDIT_NOTE_REQUEST);
                }
            });

        } else {

            noteListAdapter.setOnItemClickListener(new NoteListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Note note) {
                    Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
                    intent.putExtra(AddEditNoteActivity.EXTRA_ID, note.getId());
                    intent.putExtra(AddEditNoteActivity.EXTRA_TITLE, note.getTitle());
                    intent.putExtra(AddEditNoteActivity.EXTRA_DESCRIPTION, note.getDescription());
                    intent.putExtra(AddEditNoteActivity.EXTRA_PRIORITY, note.getPriority());
                    startActivityForResult(intent, EDIT_NOTE_REQUEST);
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            String title = data.getStringExtra(AddEditNoteActivity.EXTRA_TITLE);
            String description = data.getStringExtra(AddEditNoteActivity.EXTRA_DESCRIPTION);
            int priority = data.getIntExtra(AddEditNoteActivity.EXTRA_PRIORITY, 1);

            Note note = new Note(title, description, priority);

            if (requestCode == ADD_NOTE_REQUEST) {
                noteViewModel.insert(note);
                Toast.makeText(this, "Note inserted", Toast.LENGTH_SHORT).show();
            } else if (requestCode == EDIT_NOTE_REQUEST) {
                int id = data.getIntExtra(AddEditNoteActivity.EXTRA_ID, -1);

                if (id == -1) {
                    Toast.makeText(this, "Note couldn't be updated.", Toast.LENGTH_SHORT).show();
                    return;
                }

                note.setId(id);
                noteViewModel.update(note);
                Toast.makeText(this, "Note has been updated.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Note not added", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete_all_notes:
                deleteAllNotes();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void deleteAllNotes() {
        noteViewModel.deleteAll();
        Toast.makeText(this, "All Notes deleted", Toast.LENGTH_SHORT).show();
    }
}