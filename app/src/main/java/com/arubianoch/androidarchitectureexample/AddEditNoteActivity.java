package com.arubianoch.androidarchitectureexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

public class AddEditNoteActivity extends AppCompatActivity {

    public static final String EXTRA_ID = "com.arubianoch.androidarchitectureexample.EXTRA_ID";
    public static final String EXTRA_TITLE = "com.arubianoch.androidarchitectureexample.EXTRA_TITLE";
    public static final String EXTRA_DESCRIPTION = "com.arubianoch.androidarchitectureexample.EXTRA_DESCRIPTION";
    public static final String EXTRA_PRIORITY = "com.arubianoch.androidarchitectureexample.EXTRA_PRIORITY";

    private EditText newTitle, newDescription;
    private NumberPicker newPriority;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        newTitle = findViewById(R.id.add_note_title);
        newDescription = findViewById(R.id.add_note_description);
        newPriority = findViewById(R.id.add_note_priority);

        newPriority.setMinValue(1);
        newPriority.setMaxValue(10);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            setTitle("Edit Note");
            newTitle.setText(intent.getStringExtra(EXTRA_TITLE));
            newDescription.setText(intent.getStringExtra(EXTRA_DESCRIPTION));
            newPriority.setValue(intent.getIntExtra(EXTRA_PRIORITY, 1));
        } else {
            setTitle("Add Note");
        }
    }

    public void saveNote() {
        String title = newTitle.getText().toString();
        String description = newDescription.getText().toString();
        int priority = newPriority.getValue();

        if (title.trim().isEmpty() && description.trim().isEmpty()) {
            Toast.makeText(this, "Please, insert a title and a description.", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_TITLE, title);
        data.putExtra(EXTRA_DESCRIPTION, description);
        data.putExtra(EXTRA_PRIORITY, priority);

        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_add_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_note:
                saveNote();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
