package com.arubianoch.androidarchitectureexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ChooseAdapterTypeActivity extends AppCompatActivity {

    public static final String EXTRA_HOLDER_TYPE = "com.arubianoch.androidarchitectureexample.HOLDER_TYPE";
    public static final String EXTRA_RECYCLER_ADAPTER = "com.arubianoch.androidarchitectureexample.RECYCLER_ADAPTER";
    public static final String EXTRA_LIST_ADAPTER = "com.arubianoch.androidarchitectureexample.LIST_ADAPTER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_adapter_type);
    }

    public void onRecyclerAdapterButtonClicked(View view) {
        startMainActivity(EXTRA_RECYCLER_ADAPTER);
    }

    public void onListAdapterButtonClicked(View view) {
        startMainActivity(EXTRA_LIST_ADAPTER);
    }

    private void startMainActivity(String param) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(EXTRA_HOLDER_TYPE, param);
        startActivity(intent);
    }
}
